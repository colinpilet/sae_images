from PIL import Image

i = Image.open("hall-mod_0.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        sortie.putpixel((-x, y), c)
sortie.save("Imageout1.bmp")