from PIL import Image

def cacher(i, b):
    return i-(i%2)+b

noir = (0, 0, 0)
blanc = (255, 255, 255)

i = Image.open("Imageout_steg_0.bmp")
cache = Image.open("Imageout3.bmp")
sortie = i.copy()
for y in range (cache.size[1]):
    for x in range(cache.size[0]):
        c = i.getpixel((x, y))
        d = cache.getpixel((x,y))
        r, v, b = c
        if d == noir:    
            sortie.putpixel((x, y), (cacher(r,1), v, b))
        else:
            sortie.putpixel((x, y), (cacher(r,0), v, b))
sortie.save("Imageout_steg_1.bmp")