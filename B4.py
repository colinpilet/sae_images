from PIL import Image
noir = (0, 0, 0)
blanc = (255, 255, 255)

i = Image.open("IUT-Orleans.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        r, v, b = c
        if (r*r+v*v+b*b) > 255*255*3/2:
            couleur = blanc 
        else:
            couleur = noir       
        sortie.putpixel((x, y), couleur)
sortie.save("Imageout3.bmp")