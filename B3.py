from PIL import Image

i = Image.open("IUT-Orleans.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        r, v, b = c
        gris = (r+v+b)//3
        sortie.putpixel((x, y), (gris, gris, gris))
sortie.save("Imageout2.bmp")