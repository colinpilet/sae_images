from PIL import Image


i = Image.open("hall-mod_0.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        r, v, b = c     
        sortie.putpixel((x, y), (r-r%2, v, b))
sortie.save("Imageout_steg_0.bmp")
