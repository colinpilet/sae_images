# SAE Image

## Réponse aux questions

### A.0
L'erreur provient de la taille du fichier que n'est pas la bonne. 

![](./image/A0_0.png)

Il faut donc modifier cette valeur, la mettre à 9A 73 0C.

![](./image/A0_1.png)

### A.1
Voici le resultat obtenue:

![](./image/Image0.png)

Voici le code hexadecimal:

![](./image/Image0Hex.png)

### A.2
Voici le resultat obtenue:

![](./image/Imagetest.png)

Voici le code hexadecimal:

![](./image/ImagetestHex.png)

### A.3
Voici le resultat obtenue:

![](./image/Image1.png)

Voici le code hexadecimal:

![](./image/Image1Hex.png)

L'image a un poid de 102 octets.

1) Il y a 24 bits par pixel car on utilise 3 octet pour un pixel.
2) Les données pixel représente 4*4*3 octets soit 48 octets.
3) Il n'y a pas de compression utiliser, on peut voir cela en regardant a l’adresse 1E sur 4 octet de l’image 1. Cet octet étant à 0 indique qu'il n'y a pas de compression. 
4) Le codage des pixels n’a pas changé, par contre la taille du header est plus grande sur l’image1.bmp.
### A.4
Voici le resultat obtenue pour la convertion en mode index de couleur:

![](./image/Image2.png)

Voici le code hexadecimal:

![](./image/Image2Hex.png)

1) Il y a 4 bits par pixel
2) La taille des données pixels est de 48 octets
3) il n'y a pas de compression
4)
5) Il y a 2 couleurs dans la palette, du blanc et du rouge.
6)
7) Voici l'image bleue et le code de celle-ci:

![](./image/ImageBleue.png)

![](./image/ImageBleueHex.png)

9) Voici l'image 3 et le code de celle-ci:

![](./image/Image3.png)

![](./image/Image3Hex.png)

10) 

![](./ImageExempleIndexBMP3_16-Droit.bmp)

11) On peut trouver le nombre de couleurs de la palette à l'adresse 2E. Ici il y a 16 couleurs.

![ImageExempleIndexBMP3_16.bmp](./image/ImageExempleIndex16.png)

12) La couleur à dominante "Blanc" ce trouve à l'adresse 66 coder sur 4 octets en little indian : FE FE FD

13) Le tableau de pixel commence à l'adresse 76

14) 
![](pixelbleu.png)

![](pixelbleueHex.png)

J'ai rejouté ici 10 pixels bleu, représenté par les octets EE

15) Lorsque l'on diminue le nombre de couleurs dans la palette on ne peut plus voir certaines couleurs, l'image est donc déteriorée
Dans l'hexadecimal, on remarque qu'il ne reste que 4 couleurs et que celles étant précédemment présentes sont remplcer par des octes à 00

![](./image/ImageExempleIndex4.png)

### A.5

![](./image/Image3invert.png)

![](./image/Image3Hexinvert.png)

En changeant la valeurde la hauteur par une valeur négative l'image est inversée.

![](./image/Image3.png)

![](ImageExempleIndexBMP3_16.bmp)

### A.6

1) Le fichier fait 1120 octets. La compression RLE créé automatiquement une palette de 256 couleurs.
2) Pour savoir quelle est l’adresse du debut des pixels il faut regarder a l’adresse 0A sur 4 octets. On obtiens alors l’adresse 00 00 04 36.
3) Un pixel est codé sur 2 octet 1 octet indiquant le nombre de pixel à prendre en compte et 1 octet indiquant la couleur des pixel. Cette couleur est choisi parmis celle de la palette.

![](./image/Image4.png)
![](./image/image4pixelHex.png)

Ici un pixel rouge est coder par les octets 01 00 et un pixel blanc par 01 01. Pour passer d'une ligne à l'autre, il faut mettre c'est 2 octets à 0 --> 00 00. A la fin du codage des pixel il faut mettre les octets 00 01.

### A.7

1) Le poid du fichier est 044E soit 1102 octets. Le poid est moins grand que l'image 4 car le format RLE permet de coder plusieurs pixels en 2 octets, 1 octet pour indiquer le nombre de pixel et un autre pour la couleur. Ici comme nous avons 3 lignes de couleur unie cela réduit donc le code, car nous n'avons pas à coder chaque pixels.

![](./image/Image5.png)
![](./image/image5pixelHex.png)

Les lignes rouge sont coder par les octets 04 00

### A.8
Voici le resultat obtenue:

![](./image/Image6.png)

Voici le code hexadecimal des pixels:

![](./image/Image6Hex.png)

### A.9
Voici le resultat obtenue:

![](./image/Image7.png)

Voici le code hexadecimal de la palettes de couleur:

![](./image/Image7couleurs.png)

Voici le code hexadecimal des pixels:

![](./image/Image7Hex.png)

Il faut rejouter les nouvelles couleurs dans la palette de couleurs. Ainsi, ici le vert sera 00 FF 00 et le bleu FF 00 00. Il ne faut pas oublier de mettre un octet à 00 entre chaque couleur.

### A.10

![](./image/Image8.png)
![](./image/Image8Hex.png)

Une fois toutes les couleurs qui ne servent à rien on obtient ce code:

![](./image/Image9Hex.png)

### B.1

Afin de transposer l'image Test, il faut inverser l'axe X et l'axe Y. 
Voici le code à utiliser:
```
from PIL import Image

i = Image.open("Imagetest.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        sortie.putpixel((y, x), c)
sortie.save("Imageout0.bmp")
```

![](./image/Imageout0.png)

### B.2

Voici le code utiliser pour mettre l'image en miroir:
```
from PIL import Image

i = Image.open("hall-mod_0.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        sortie.putpixel((-x, y), c)
sortie.save("Imageout1.bmp")
```

Il faut inversé l'axe X.
On obtient alors l'image suivante:

![](./Imageout1.bmp)

### B.3

Voici le code pour passer une image en niveau de gris:
```
from PIL import Image

i = Image.open("IUT-Orleans.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        r, v, b = c
        gris = (r+v+b)//3
        sortie.putpixel((x, y), (gris, gris, gris))
sortie.save("Imageout2.bmp")
```
On obtient cette image :

![](./Imageout2.bmp)

### B.4

Voici le code pour passer une image en noir et blanc:
```
from PIL import Image
noir = (0, 0, 0)
blanc = (255, 255, 255)

i = Image.open("IUT-Orleans.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        r, v, b = c
        if (r*r+v*v+b*b) > 255*255*3/2:
            couleur = blanc 
        else:
            couleur = noir       
        sortie.putpixel((x, y), couleur)
sortie.save("Imageout3.bmp")
```
On obtient cette image :

![](./Imageout3.bmp)

### B.5

Voici le code pour mettre toutes les valeurs rouge à une valeur paire :
```
from PIL import Image

i = Image.open("hall-mod_0.bmp")
sortie = Image.new(i.mode, i.size)
sortie.putdata(i.getdata())
for y in range (i.size[1]):
    for x in range(i.size[0]):
        c = i.getpixel((x, y))
        r, v, b = c     
        sortie.putpixel((x, y), (r-r%2, v, b))
sortie.save("Imageout_steg_0.bmp")
```

Voici le code pour cacher le logo dans l'image:
```
from PIL import Image

def cacher(i, b):
    return i-(i%2)+b

noir = (0, 0, 0)
blanc = (255, 255, 255)

i = Image.open("Imageout_steg_0.bmp")
cache = Image.open("Imageout3.bmp")
sortie = i.copy()
for y in range (cache.size[1]):
    for x in range(cache.size[0]):
        c = i.getpixel((x, y))
        d = cache.getpixel((x,y))
        r, v, b = c
        if d == noir:    
            sortie.putpixel((x, y), (cacher(r,1), v, b))
        else:
            sortie.putpixel((x, y), (cacher(r,0), v, b))
sortie.save("Imageout_steg_1.bmp")
```

Voici le code pour trouver le logo dans l'image:
```
from PIL import Image

def trouver(i):
    return i%2

noir = (0, 0, 0)
blanc = (255, 255, 255)

i = Image.open("Imageout_steg_1.bmp")
sortie = Image.new(i.mode, i.size)
cache = Image.open("Imageout3.bmp")
sortie.putdata(i.getdata())
for y in range (cache.size[1]):
    for x in range(cache.size[0]):
        c = i.getpixel((x, y))
        r, v, b = c 
        if trouver(r) == 0:
            couleur = blanc
        else:
            couleur = noir    
        sortie.putpixel((x, y), couleur)
sortie.save("Imageout_steg_2.bmp")
```

On obtient alors cette image :

![](./Imageout_steg_2.bmp)

## Ce que j'ai appris

