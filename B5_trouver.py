from PIL import Image

def trouver(i):
    return i%2

noir = (0, 0, 0)
blanc = (255, 255, 255)

i = Image.open("Imageout_steg_1.bmp")
sortie = Image.new(i.mode, i.size)
cache = Image.open("Imageout3.bmp")
sortie.putdata(i.getdata())
for y in range (cache.size[1]):
    for x in range(cache.size[0]):
        c = i.getpixel((x, y))
        r, v, b = c 
        if trouver(r) == 0:
            couleur = blanc
        else:
            couleur = noir    
        sortie.putpixel((x, y), couleur)
sortie.save("Imageout_steg_2.bmp")